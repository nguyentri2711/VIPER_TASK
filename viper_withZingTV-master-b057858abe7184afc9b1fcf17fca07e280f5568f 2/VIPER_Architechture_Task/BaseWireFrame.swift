//
//  BaseWireFrame.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 13/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit

class BaseWireFrame{
    class func createMainModule() -> UIViewController {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController")
            return viewController
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
